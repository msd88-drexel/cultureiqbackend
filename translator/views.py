from django.utils.timezone import now
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from translator.models import translation_request
from watson_developer_cloud import LanguageTranslatorV2 as LanguageTranslator
from cultureIqBackend.settings import username, password
from langdetect import detect

import json

@api_view(['get'])
def list_languages(request):
    translator = LanguageTranslator(
        username=username,
        password=password
    )

    languages = translator.get_identifiable_languages()
    return Response(languages)


@api_view(['post'])
def translate(request):
    request_json = json.loads(request.body.decode('utf-8'))

    translator = LanguageTranslator(
        username=username,
        password=password
    )

    translate_res = translator.translate(
        request_json['src_tx'],
        request_json['src_lang'],
        request_json['dst_lang']
    )

    translation_request(
        souce_tx=request_json['src_tx'],
        request_date=now(),
        dst_lang_tx=request_json['dst_lang'],
        src_lang_tx=request_json['src_lang'],
        trans_tx=translate_res,
        user_id=request.user
    ).save()

    return Response(translate_res)

@api_view(['get'])
def translation_history(request):
    translations = translation_request.objects \
    .filter(user_id=request.user.id) \
    .order_by('-request_date') \
    [:10000]

    translation_data = map(lambda x: x.to_dict(), translations)

    return Response(translation_data)

@api_view(['post'])
def detect_language(request):
    return Response(detect(str(request.body)))

@api_view(['post'])
def get_translatable_langs(request):
    translator = LanguageTranslator(
        username=username,
        password=password
    )

    translatable_langs = translator.get_models(default=True, source=request.body)

    return Response(map(lambda lang: lang['target'], translatable_langs['models']))

