from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class translation_request(models.Model):
    request_date = models.DateTimeField(null=False)
    souce_tx = models.TextField(null=False)
    trans_tx = models.TextField(null=False)
    src_lang_tx = models.TextField(null=False)
    dst_lang_tx = models.TextField(null=False)
    user_id = models.ForeignKey(User, unique=False)

    def to_dict(self):
        return {
            'request_date': self.request_date,
            'source_tx':    self.souce_tx,
            'trans_tx':     self.trans_tx,
            'src_lang_tx':  self.src_lang_tx,
            'dst_lang_tx':  self.dst_lang_tx
        }

