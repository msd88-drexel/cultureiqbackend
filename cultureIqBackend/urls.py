"""cultureIqBackend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from rest_framework_jwt.views import obtain_jwt_token

from translator.views import *

urlpatterns = [
    url(r'^list_languages', list_languages),
    url(r'^translate', translate),
    url(r'^translation_history', translation_history),
    url(r'^detect_language', detect_language),
    url(r'^api-token-auth', obtain_jwt_token),
    url(r'^translatable_langs', get_translatable_langs)

]
